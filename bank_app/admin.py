from django.contrib import admin
from .models import Account, Loan, Rank, CustomUser, Ledger

admin.site.register(Account)
admin.site.register(Rank)
admin.site.register(CustomUser)
admin.site.register(Loan)
admin.site.register(Ledger)
