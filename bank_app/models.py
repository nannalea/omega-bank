from decimal import Decimal
from django.db import models, transaction
from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from .errors import InsufficientFunds
from django.db.models import Q
import uuid


class Rank(models.Model):
    name = models.CharField(max_length=30, unique=True, db_index=True)
    value = models.IntegerField(unique=True)

    def __str__(self):
        return self.name


class CustomUser(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.PROTECT)
    phone = models.CharField(max_length=12, db_index=True)
    rank = models.ForeignKey(Rank, default=1, on_delete=models.PROTECT)
    isActive = models.BooleanField(default=True)

    @property
    def full_name(self) -> str:
        return f'{self.user.first_name} {self.user.last_name}'

    @property
    def accounts(self) -> QuerySet:
        return Account.objects.filter(user=self.user, isLoan=False, isActive=True)

    @property
    def accounts_and_loans(self):
        return Account.objects.filter(user=self.user, isActive=True)

    @property
    def loans(self) -> QuerySet:
        return Account.objects.filter(user=self.user, isLoan=True, isActive=True)

    @property
    def default_account(self):
        return Account.objects.filter(user=self.user).first()

    @property
    def can_make_loan(self) -> bool:
        return self.rank.value >= 20

    def take_loan(self, accountName, amount):
        assert self.can_make_loan, 'Your rank does not allow this action'
        Loan.objects.create(amount=amount, user=self.user, accountName=accountName)

    @classmethod
    def search(cls, search):
        return cls.objects.filter(
            Q(user__username__contains=search) |
            Q(user__first_name__contains=search) |
            Q(user__last_name__contains=search) |
            Q(user__email__contains=search) |
            Q(phone__contains=search)
        )[:15]

    @property
    def count_self_created_accounts(self) -> int:
        return Account.objects.filter(user=self.user, createdBy=self.user).count()


class Account(models.Model):
    id = models.AutoField(primary_key=True, auto_created=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='owner')
    createdBy = models.ForeignKey(User, on_delete=models.PROTECT, null=True, related_name='creator')
    createdAt = models.DateTimeField(auto_now_add=True, db_index=True)
    accountName = models.CharField(max_length=50)
    isActive = models.BooleanField(default=True)
    isLoan = models.BooleanField(default=False)

    class Meta:
        get_latest_by = 'pk'

    @property
    def movements(self) -> QuerySet:
        return Ledger.objects.filter(account=self)

    @property
    def balance(self) -> Decimal:
        return self.movements.aggregate(models.Sum('amount'))['amount__sum'] or Decimal(0)

    @property
    def change_rank(self):
        movements = Ledger.objects.filter(account=self, isLoan=False)
        return movements.aggregate(models.Sum('amount'))['amount__sum'] or Decimal(0)

    def __str__(self):
        return f'{self.accountName} - {self.id}'


class Loan(models.Model):
    approved = models.BooleanField(default=False)
    declined = models.BooleanField(default=False)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    accountName = models.CharField(max_length=50, default='Loan')

    @classmethod
    def approve_loan(cls, loan_id):
        loan = Account.objects.create(user=loan_id.user, accountName=loan_id.accountName, isLoan=True)
        Ledger.transfer(
            loan_id.amount,
            loan,
            'Loan paid out to account',
            loan_id.user.customuser.default_account,
            f'Credit from {loan.accountName}',
            is_loan=True
        )


class Ledger(models.Model):
    transactionId = models.CharField(max_length=100)
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    text = models.CharField(max_length=100)
    createdAt = models.DateTimeField(auto_now_add=True, db_index=True)
    isLoan = models.BooleanField(default=False)

    class Meta:
        ordering = ('-createdAt',)

    @classmethod
    def transfer(cls, amount, debit_account, debit_text, credit_account, credit_text, is_loan=False) -> int:
        with transaction.atomic():
            if debit_account.balance >= amount or is_loan:
                id = uuid.uuid4()
                cls(amount=-amount, transactionId=id, account=debit_account, text=debit_text, isLoan=is_loan).save()
                cls(amount=amount, transactionId=id, account=credit_account, text=credit_text, isLoan=is_loan).save()
            else:
                raise InsufficientFunds
        return id

    @classmethod
    def bank_to_bank_transfer(cls, idempotency_token, amount, debit_account, debit_text, credit_account, credit_text, is_loan=False) -> int:
        with transaction.atomic():
            if debit_account.balance >= amount or is_loan:
                cls(amount=-amount, transactionId=idempotency_token, account=debit_account, text=debit_text, isLoan=is_loan).save()
                cls(amount=amount, transactionId=idempotency_token, account=credit_account, text=credit_text, isLoan=is_loan).save()
            else:
                raise InsufficientFunds
        return idempotency_token

    def __str__(self):
        return f'{self.amount} :: {self.createdAt} :: {self.account}'
